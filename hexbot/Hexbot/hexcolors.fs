module HexColorTools

// for string manipulations
open System

// splits a color "#RRGGBB" into a tuple ("RR", "GG", "BB")
let splitHexColorParts (color:String) = ( color.Substring(1, 2), color.Substring(3, 2), color.Substring(5, 2) )

let hexDigitValue digit =
    match digit with
    | "0" -> 0
    | "1" -> 1
    | "2" -> 2
    | "3" -> 3
    | "4" -> 4
    | "5" -> 5
    | "6" -> 6
    | "7" -> 7
    | "8" -> 8
    | "9" -> 9
    | "A" -> 10
    | "B" -> 11
    | "C" -> 12
    | "D" -> 13
    | "E" -> 14
    | "F" -> 15
    | _ -> 0

// simpe algorithm for reference: https://www.binaryhexconverter.com/hex-to-decimal-converter
// hex is a 2 character string representing a color in hexadecimal format
// i.e. "FF" which is 255
let hex2dec (hex:String) =
    (hex.Substring(0, 1) |> hexDigitValue) * (pown 16 1) + (hex.Substring(1, 1) |> hexDigitValue) * (pown 16 0)
