module Hexbot

// my utilities are in there
open HexColorTools

// for HTTP requests
open FSharp.Data
// good reference about Data library
// https://fsharp.github.io/FSharp.Data/library/JsonValue.html
// https://fsharp.github.io/FSharp.Data/library/JsonProvider.html


(*********************************************
just fooling arround with
https://noopschallenge.com/challenges/hexbot
**********************************************)

// feed info
let noopEndpoint = "https://api.noopschallenge.com/hexbot?count=10"
let jsonHeaders = ["Accept", "application/json"]


// parser of Json answer into JsonProvider type
type Answer = JsonProvider<""" { "colors": [ {"value": "#52a351"} ] } """>
// type Color = JsonProvider<""" {"value": "#52a351"} """> // in fact, that definition is not needed


// fetch one random color
let MakeColorRequest =
    Http.RequestString ( noopEndpoint, httpMethod = "GET", headers = jsonHeaders )


// just keep the color values from the Array of colors
let ExtractColors ans =
    Answer.Parse ans
    |> (fun x -> x.Colors)
    |> Array.map (fun x -> x.Value)


let makeHexCube hexcolor =
    let (rHex, gHex, bHex) = splitHexColorParts hexcolor
    let r = hex2dec rHex
    let g = hex2dec gHex
    let b = hex2dec bHex
    sprintf "\u001b[38;2;%i;%i;%im■\u001b[0m" r g b


// yeah, Powershell supports ansi escape codes!
// the color needs to be with format "#RRGGBB"
let printColor color =
    printfn "%s %s" (makeHexCube color) color


let sortColors colors =
    printfn "Before: %A" colors
    Array.sort colors |> printfn "After: %A"


[<EntryPoint>]
let main argv =
    MakeColorRequest  // simply load a color from hexbot
    |> ExtractColors
    // |> Array.map printColor // one option is to print the colors
    |> sortColors // one option is to sort the colors!
    |> ignore

    printf "Press a key..."
    System.Console.ReadKey() |> ignore
    0 // return an integer exit code
